from typing import List
from collections import defaultdict
from coffea import processor
import awkward as ak
import numpy as np
from hist import Hist
import hist
import boost_histogram as bh
import vector
from utils.Selections import make_cuts
import src.PhotonShowerDepths as phsh_depth
from numpy import sinh
from utils.utils import flat_ntuple

class DiphotonProcessor(processor.ProcessorABC):

    def __init__(self, 
                regions: List[str] = ['SR'],              
                extra_args: str = ""
                ):

        self.hist = Misc.get_histograms(regions)
        self.regions = regions
        self.arguments = extra_args
        self.psd = phsh_depth.PhotonShowerDepth("/home/cossio/data_ceph/PhotonShowerDepthFiles/", "LooseConv", "EtaBaryRes")
       
    def process(self, events):
        
        dataset = events.metadata['dataset']
        
        for region in self.regions:
            events_cut = make_cuts(events, region, dataset)
            self.outputs = events_cut
            
            self.outputs = Calculate.shower_depths(self.outputs,self.psd)
            self.outputs = Misc.get_tlorentz(self.outputs)
            
            histograms = Misc.fill_histograms(self.hist, self.outputs, region)
            

        flat_ntuple(self.outputs)
        
        return {
            'histograms': {dataset: histograms}
        }

    def postprocess(self, accumulator):
        return accumulator

class Calculate():

    def shower_depths(outputs, psd):
        R1, R2, ptg = [],[],[]
        
        for (eta1e1, eta2e1, E1, eta1e2, eta2e2, E2) in zip( \
            outputs.ph.caloCluster_etas1[:,0], outputs.ph.caloCluster_etas2[:,0], outputs.ph.E[:,0], \
            outputs.ph.caloCluster_etas1[:,1], outputs.ph.caloCluster_etas2[:,1], outputs.ph.E[:,1]):
                
            r1_e1_, r2_e1_ = psd.phsh_depth(eta1e1, eta2e1, E1)
            r1_e2_, r2_e2_ = psd.phsh_depth(eta1e2, eta2e2, E2)

            ptg_e1_ = r1_e1_ * r2_e1_ * (sinh(eta1e1) -  sinh(eta2e1)) / (r2_e1_ - r1_e1_)
            ptg_e2_ = r1_e2_ * r2_e2_ * (sinh(eta1e2) -  sinh(eta2e2)) / (r2_e2_ - r1_e2_)
            
            ptg.append([ptg_e1_, ptg_e2_])
            R1.append([r1_e1_, r1_e2_])
            R2.append([r2_e1_, r2_e2_])
            
        outputs["ph", "R1"] = R1
        outputs["ph", "R2"] = R2
        outputs["ph", "pointing"] = ptg
        
        return outputs

class Misc():

    def get_tlorentz(events):
        
        _e_1_pt = ak.Array([])
        _e_1_eta = ak.Array([])
        _e_1_phi = ak.Array([])
        _e_1_E = ak.Array([])
        _e_2_pt = ak.Array([])
        _e_2_eta = ak.Array([])
        _e_2_phi = ak.Array([])
        _e_2_E = ak.Array([])

        _e_1_pt = events.ph.pt[:,0]
        _e_1_eta = events.ph.eta[:,0]
        _e_1_phi = events.ph.phi[:,0]
        _e_1_E = events.ph.E[:,0]
        _e_2_pt = events.ph.pt[:,1]
        _e_2_eta = events.ph.eta[:,1]
        _e_2_phi = events.ph.phi[:,1]
        _e_2_E = events.ph.E[:,1]
        
        e_1 = {
            "pt": _e_1_pt,
            "phi": _e_1_phi,
            "eta": _e_1_eta,
            "E" : _e_1_E,
            }
        
        e_2 = {
            "pt": _e_2_pt,
            "phi": _e_2_phi,
            "eta": _e_2_eta,
            "E": _e_2_E,
            }
        
        events["ph", "ee_mass"] = (vector.arr(e_1) + vector.arr(e_2)).mass
        events["ph", "ee_E"] = (vector.arr(e_1) + vector.arr(e_2)).E
        events["ph", "ee_pt"] = (vector.arr(e_1) + vector.arr(e_2)).pt
        
        return events

    
    def get_histograms(regions: List[str]):
        
        histo = dict()
       
        histo['ph_1'] = (
            Hist.new.StrCat(regions, name="region")
                        .Reg(50, 1000, 1600, name="ph_1_R1")
                        .Reg(50, 1000, 1800, name="ph_1_R2")
                        .Reg(50, -200, 200, name="ph_1_ptg")
                        .Weight()
        )
        
        histo['ph_2'] = (
            hist.Hist.new.StrCat(regions, name="region")
                        .Reg(50, 1000, 1600, name="ph_2_R1")
                        .Reg(50, 1000, 1800, name="ph_2_R2")
                        .Reg(50, -200, 200, name="ph_2_ptg")
                        .Weight()
        )
        histo['yy'] = (
            hist.Hist.new.StrCat(regions, name="region")
                        .Reg(50, 1, 300, name="yy_pt")
                        .Reg(50, 1, 300, name="yy_E")
                        .Reg(100, 0, 300, name="yy_mass")
                        .Weight()
        )
        return histo
    
    def fill_histograms(hist, outputs, region):
       
        histograms = hist.copy()
        
        histograms['ph_1'].fill(
            region=region,
            ph_1_R1 = outputs.ph.R1[:,0],
            ph_1_R2 = outputs.ph.R2[:,0],
            ph_1_ptg = outputs.ph.pointing[:,0],
            weight = np.ones(len(outputs)))

        histograms['ph_2'].fill(
            region=region,
            ph_2_R1 = outputs.ph.R1[:,1],
            ph_2_R2 = outputs.ph.R2[:,1],
            ph_2_ptg = outputs.ph.pointing[:,1],
            weight = np.ones(len(outputs)))

        histograms['yy'].fill(
            region=region,
            yy_pt = outputs.ph.ee_pt[:,0],
            yy_E = outputs.ph.ee_E[:,0],
            yy_mass = outputs.ph.ee_mass[:,0],
            weight = np.ones(len(outputs)))

        return histograms 
          