import os 
import glob
import shutil
from servicex_databinder import DataBinder

class ServiceXDelivery():
    def __init__(self, sample):
        self.file =  r'servicex/config/{}.yaml'.format(sample)
        self.sx_db = DataBinder(self.file)
    
    def deliver(self):
        out = self.sx_db.deliver()
        fileset_dict = self.get_dict()
        return fileset_dict

    def get_dict(self):
        """
        Moves files from tmp to avoid them getting deleted from failed ServiceX requests'
        Outputs the dictionary
        """
        f = open(self.file)
        lines = f.readlines()

        
        deliv_dir = lines[2].replace("  OutputDirectory: ", "").replace("\n","")
        tree = "CollectionTree"
        f.close()
        
        sample_dirs = glob.glob(deliv_dir + "/*/"+ tree)
        
        fileset_dict = {}
        for sample_dir in sample_dirs:
            deliv_files = glob.glob(sample_dir + "/*.root")
            dest_dir = sample_dir.replace("_tmp","").replace(tree, "")
            isExist = os.path.exists(dest_dir)
            if not isExist:
                os.makedirs(dest_dir)
            for file in deliv_files:
                file_name = os.path.basename(file)
                shutil.move(file, dest_dir + file_name)
            sample = sample_dir.replace(deliv_dir, "").replace(tree, "").replace("/","")
        
            files = glob.glob(dest_dir + "/*.root")
            fileset_dict[sample] = files

        return fileset_dict
    

        

        
        

        

        