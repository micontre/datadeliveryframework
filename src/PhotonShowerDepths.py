import uproot
import boost_histogram as bh

class PhotonShowerDepth:

    def __init__(self, dir, photon_type, calc_method):
        self.dir = dir
        self.pt = photon_type
        self.cm =  calc_method
        if photon_type.find("Tight") == 0: self.ext = "Fine"
        else: self.ext = "Coarse"
        self.phsh_hists()

    def phsh_hists(self):
        filepath = r'{}/FittedDepths_{}_{}.{}.LowOrderFits.LCFIT.ALL.root'.\
            format(self.dir, self.pt, self.ext, self.cm)
        self.depth_file = uproot.open(filepath)
        self.depth_hist_1 = self.depth_file["h2_R1"].to_boost()
        self.depth_hist_2 = self.depth_file["h2_R2"].to_boost()

    def phsh_depth(self, eta1, eta2, E):
        #el = 0
        #print(outputs.el.etas1[:,el])
        R1 = self.depth_hist_1[bh.loc(eta1), bh.loc(E)].value
        R2 = self.depth_hist_2[bh.loc(eta2), bh.loc(E)].value
        #print(R1)
        #print(type(R1))
        return R1, R2