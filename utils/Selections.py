import awkward as ak

def make_cuts(events, region_name,dataset):
    """
    cut_diphoton_trigger_1 = events.HLT.vals[:,1]==1
    cut_diphoton_trigger_2 = events.HLT.vals[:,2]==1
       
    #apply 1st cuts
    events = events[cut_diphoton_trigger_1 | cut_diphoton_trigger_2]
    
    #opposite sign selection
    cut_electron_sign = ak.sum(events.el.q, axis=1) == 0
    #apply 2nd cuts
    events = events[cut_electron_sign]

    #electron identification 
    cut_electron_id_1 = events.el.isMedium[:,0]*1==1
    cut_electron_id_2 = events.el.isMedium[:,1]*1==1
    #apply 3rd cutsn
    events = events[cut_electron_id_1 & cut_electron_id_2]
    """
    
    cut_two_photons = ak.num(events.ph) == 2
    
    events = events[cut_two_photons]
    
    cut_diphoton_trigger_1 = events.HLT.vals[:,1]==1
    cut_diphoton_trigger_2 = events.HLT.vals[:,2]==1
       
    events = events[cut_diphoton_trigger_1 | cut_diphoton_trigger_2]

    #photon id
    cut_photon_id_1 = events.ph.isMedium[:,0]*1==1
    cut_photon_id_2 = events.ph.isMedium[:,1]*1==1
    #apply 3rd cutsn
    events = events[cut_photon_id_1 & cut_photon_id_2]
        
    return events