import matplotlib.pyplot as plt
import mplhep as hep

class Plot():
    def __init__(self, events, dataset, plot):
        self.events = events
        self.dataset = dataset
        if plot=="all":
            #self.ee_plots()
            #self.e1_plots()
            #self.e2_plots()
            self.yy_plots()
            self.ph1_plots()
            self.ph2_plots()
    
    def ee_plots(self):
        ee_hist = self.events['histograms'][self.dataset]['ee']
        Plotter.plotter_2d(ee_hist,xlabel = "$M_{ee}$ [GeV]", projection="ee_mass", year="2017")
        
    def e1_plots(self):
        e1_hist = self.events['histograms'][self.dataset]['e_1']
        Plotter.plotter_2d(e1_hist, xlabel = "$R1_{e1}$ [mm]", projection="e_1_R1", year="2017", layer="front")
        Plotter.plotter_2d(e1_hist, xlabel = "$R2_{e1}$ [mm]", projection="e_1_R2", year="2017", layer="middle")
        Plotter.plotter_2d(e1_hist, xlabel = "$e_1 Pointing [mm]$", projection="e_1_ptg", year="2017")
        
    def e2_plots(self):
        e2_hist = self.events['histograms'][self.dataset]['e_2']
        Plotter.plotter_2d(e2_hist, xlabel = "$R1_{e2}$ [mm]", projection="e_2_R1", year="2017", layer="front")
        Plotter.plotter_2d(e2_hist, xlabel = "$R2_{e2}$ [mm]", projection="e_2_R2", year="2017", layer="middle")
        Plotter.plotter_2d(e2_hist, xlabel = "$e_2 Pointing [mm]$", projection="e_2_ptg", year="2017")
       
    def yy_plots(self):
        yy_hist = self.events['histograms'][self.dataset]['yy']
        Plotter.plotter_2d(yy_hist,xlabel = "$M_{\gamma \gamma }$ [GeV]", projection="yy_mass", year="2017")
        
    def ph1_plots(self):
        ph1_hist = self.events['histograms'][self.dataset]['ph_1']
        Plotter.plotter_2d(ph1_hist, xlabel = "$R1_{\gamma_1}$ [mm]", projection="ph_1_R1", year="2017", layer="front")
        Plotter.plotter_2d(ph1_hist, xlabel = "$R2_{\gamma_1}$ [mm]", projection="ph_1_R2", year="2017", layer="middle")
        Plotter.plotter_2d(ph1_hist, xlabel = "$\gamma_1 Pointing [mm]$", projection="ph_1_ptg", year="2017")
        
    def ph2_plots(self):
        ph2_hist = self.events['histograms'][self.dataset]['ph_2']
        Plotter.plotter_2d(ph2_hist, xlabel = "$R1_{\gamma_2}$ [mm]", projection="ph_2_R1", year="2017", layer="front")
        Plotter.plotter_2d(ph2_hist, xlabel = "$R2_{\gamma_2}$ [mm]", projection="ph_2_R2", year="2017", layer="middle")
        Plotter.plotter_2d(ph2_hist, xlabel = "$\gamma_2 Pointing [mm]$", projection="ph_2_ptg", year="2017")
       
        

class Plotter():        
    def plotter_2d(hist, projection, year, xlabel, ylabel="$N_{events}$", layer="N/A"):

        fig, axs = plt.subplots(figsize=(8,6))

        plt.yscale("log") 
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)

        hep.histplot(hist.project(projection), color="cornflowerblue")

        if layer == "front":
            plt.axvline(1500, color='r', linestyle='dashed')
            plt.axvline(1590.63, color='r',linestyle='dashed')
        elif layer == "middle":
            plt.axvline(1590.63, color='r',linestyle='dashed')
            plt.axvline(1927.85, color='r',linestyle='dashed')

        plt.style.use([hep.style.ROOT, hep.style.firamath])
        hep.atlas.label(loc=0, data=True, label="Internal",year=year)
        plt.show()
    