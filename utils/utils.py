import os
#from ROOT import *
#import numpy as np
import uproot

def get_fileset(datasets, data_name):
    files = []
    fileset = dict()
    for dataset in datasets:
        dir_path = r'/home/cossio/data_ceph/LLP/Zee/data_files/{}'.format(dataset)
        for path in os.listdir(dir_path):
            if os.path.isfile(os.path.join(dir_path, path)):
                files.append("{}/{}".format(dir_path,path))
    fileset[data_name] = files
    return fileset

def flat_ntuple(output):
    
    ntuple = {
        "el1_etas1" : output.el.etas1[:,0],
        "el1_etas2" : output.el.etas2[:,0],
        "el2_etas1" : output.el.etas1[:,1],
        "el2_etas2" : output.el.etas2[:,1],
        "el1_phis1" : output.el.phis1[:,0],
        "el1_phi2" : output.el.phis2[:,0],
        "el2_phis1" : output.el.phis1[:,1],
        "el2_phis2" : output.el.phis2[:,1],
        "el1_maxEcell_E" : output.el.maxEcell_E[:,0],
        "el2_maxEcell_E" : output.el.maxEcell_E[:,1],
        "el1_maxEcell_x" : output.el.maxEcell_x[:,0],
        "el2_maxEcell_x" : output.el.maxEcell_x[:,1],
        "el1_maxEcell_y" : output.el.maxEcell_y[:,0],
        "el2_maxEcell_y" : output.el.maxEcell_y[:,1],
        "el1_maxEcell_z" : output.el.maxEcell_z[:,0],
        "el2_maxEcell_z" : output.el.maxEcell_z[:,1],
        "el1_maxEcell_t" : output.el.maxEcell_t[:,0],
        "el2_maxEcell_t" : output.el.maxEcell_t[:,1],
        "dEta_el" : output.el.eta[:,0] - output.el.eta[:,1],
        "el1_eta" : output.el.eta[:,0],
        "el2_eta" : output.el.eta[:,1],
        "el1_phi" : output.el.phi[:,0],
        "el2_phi" : output.el.phi[:,1],
        "el1_pt" : output.el.pt[:,0],
        "el2_pt" : output.el.pt[:,1],
        "el1_E" : output.el.E[:,0],
        "el2_E" : output.el.E[:,1],
        "el1_R1" : output.el.R1[:,0],
        "el1_R2" : output.el.R2[:,0],
        "el2_R1" : output.el.R1[:,1],
        "el2_R2" : output.el.R2[:,1],
        "ee_pointing" : output.el.pointing[:,0]
    }
    
    file = uproot.recreate("ntuple.root")
    file["tree"] = ntuple
    